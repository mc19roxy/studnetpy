import sys

import click
import os
import signal
from studnetpy.studnet import Studnet


class SigIntHandler:
    def __init__(self, studnet: Studnet):
        self._studnet = studnet

    def __call__(self, sigint, frame):
        click.echo("\r\nSigint catched. Exiting gracefully. Disconnecting from Studnet...")
        self.studnet.disconnect()
        click.echo(self.studnet.con_status)
        sys.exit(0)

    @property
    def studnet(self):
        return self._studnet


@click.group()
@click.pass_context
def cli(ctx):
    ctx.obj = Studnet()


@cli.command()
@click.pass_obj
@click.option('--username', "-u", required=True, type=int, envvar="STUDNETUSER", prompt=True)
@click.option("--ip", "-i", type=str, envvar="STUDNETIP", prompt=True)
@click.option('--password', '-p', type=str, envvar="STUDNETPASS", prompt=True, hide_input=True)
@click.option('--timeout', '-t', type=int, envvar="STUDNETTIMEOUT", default=5)
def connect(obj, username: int, ip: str, password: str, timeout: int):
    signal.signal(signal.SIGINT, SigIntHandler(obj))
    con(obj, username, ip, password, timeout)
    while True:
        click.option()
        choose = click.prompt("What do you want to do? (options: reconnect|disconnect|status|connected|exit)")
        if choose == "reconnect":
            new_values = click.confirm("Do you want to login with new credentials?")
            if new_values:
                username = click.prompt("Username", type=int, default=username)
                ip = click.prompt("Server Address", type=str, default=ip)
                password = click.prompt("Password", type=str, default=password, hide_input=True, show_default=False)
            disconnect(obj)
            con(obj, username, ip, password, timeout)
        if choose == "disconnect":
            disconnect(obj)
        elif choose == "status":
            statustext(obj)
        elif choose == "connected":
            connected(obj)
        elif choose == "exit":
            disconnect(obj)
            break


def con(obj, username, ip, password, timeout):
    os.environ["SSHPASS"] = password
    obj.connect(int(username), ip, mode="-e", time_out=timeout)
    click.echo(obj.con_status + "\r\n")


def disconnect(obj):
    obj.disconnect()
    click.echo(obj.con_status)


def connected(obj):
    state = obj.is_connected()
    click.echo("Currently connected to studnet: %s" % state)


def statustext(obj):
    text = obj.read_server_string()
    click.echo(text)


def status(obj):
    click.echo(obj.con_status)

