import re
from subprocess import Popen
from threading import Timer
import shlex
import subprocess
import datetime


class StatusCode:
    DISCONNECTED = 0
    CONNECTED = 1
    TIMEOUT = 2


class Studnet:
    USAGE_PATTERN = r"\[(CliStr:\d/) (?P<usage>\d+\.\d+)\s(GB)/(?P<limit>\d+)\s(GB)\]"
    TIME_STAMP_PATTERN = r"(?P<timestamp>\d+-\d+-\d+\s\d+:\d+:\d+)"
    CMD = "sshpass {mode:s} {mode_param:s} ssh -tt {user:d}@{ip:s}"
    process = None
    con_status = ""
    status = 0

    def __init__(self):
        pass

    def connect(self, user_name: int, ip: str, mode: str = "", mode_param: str = "", time_out: int = 5,
                func=lambda process: process.kill()):
        if self.is_connected():
            self.status = StatusCode.CONNECTED
            self.con_status = "Already connected."
            return 1
        command = self.CMD.format(mode=mode, mode_param=mode_param, user=user_name, ip=ip)
        command_split = shlex.split(command)
        self.process = Popen(command_split, stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        if self.process.poll() == 0:
            self.con_status = self.process.communicate()[0]
            return 0
        if self.process.poll() is not None:
            self.con_status = self.process.communicate()[1]
            return 0
        timer = Timer(time_out, func, [self.process])
        try:
            timer.start()
            self.process.stdout.peek()
        finally:
            if not timer.is_alive():
                self.con_status = "Connection timed out."
                self.status = StatusCode.TIMEOUT
                timer.cancel()
                return 0
            self.con_status = "User {:d} successfully connected to studnet server {:s}. Enjoy surfing through the www.".format(user_name, ip)
            self.status = StatusCode.CONNECTED
            timer.cancel()
            return 1

    def is_connected(self):
        return self.process is not None and self.process.poll() is None

    def read_server_string(self):
        if not self.is_connected():
            return ""
        return self.process.stdout.peek().decode("ascii")

    def disconnect(self):
        if self.is_connected():
            self.process.kill()
            self.con_status = "Disconnected from studnet."
        else:
            self.con_status = "Already disconnected."
        self.status = StatusCode.DISCONNECTED

    def timestamp(self):
        status = self.read_server_string()
        if status == "":
            return None
        match = re.search(self.TIME_STAMP_PATTERN, status)
        timestamp = match.group("timestamp")
        calculated_at = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
        return calculated_at

    def usage(self):
        status = self.read_server_string()
        if status == "":
            return None
        match = re.search(self.USAGE_PATTERN, status)
        usage = match.group("usage")
        return float(usage)

    def limit(self):
        status = self.read_server_string()
        if status == "":
            return None
        match = re.search(self.USAGE_PATTERN, status)
        limit = match.group("limit")
        return int(limit)

    def info(self):
        return {
            "calculatedAt": self.timestamp(),
            "usage": self.usage(),
            "limit": self.limit()
        }
